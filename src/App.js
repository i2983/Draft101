import './App.css';
import Welcome from "./welcome";
import Auction from "./auction";
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import {Box} from "@mui/material";

function App() {
  return (
      <Box sx={{height:'100vh'}}>
          <BrowserRouter>
            <Routes>
                <Route path={"/"} element={<Welcome/>}/>
                <Route path={"/auction/:contractaddress"} element={<Auction/>}/>
            </Routes>
          </BrowserRouter>
      </Box>
  );
}

export default App;
