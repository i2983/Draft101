import React from 'react'
import { Box, Button, TextField, Typography } from '@mui/material'


class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contractAddress: ""
        };
    }
    handleTextFieldChange(e) {
        this.setState({ contractAddress: e.target.value });
    }

    render() {
        return (
            <div>
                <div style={{
                    position: 'fixed',
                    width: '100vw',
                    height: '100vh',
                    left: 0,
                    top: 0
                }}>
                    <div id='stars'/>
                    <div id='stars2'/>
                    <div id='stars3'/>
                </div>

                <Box sx={{
                    justifyContent: "center",
                    display: 'flex',
                    alignItems: 'center',
                    height: "100vh"
                }}>
                    <Box sx={{ width: "100%", textAlign: "center", alignItems: "center", display: "flex", flexDirection: "column" }}>
                        <Box >
                            <Typography variant h1>Ready to try your luck?</Typography>
                        </Box>
                        <Box sx={{ p: 3, m: 3, width: "40vw", textAlign: "center" }}>
                            <TextField style={{
                                background: 'rgba(255,255,255,.8)'
                            }} id="outlined-basic"
                                label="Bidding contract address"
                                variant="outlined"
                                value={this.state.contractAddress}
                                fullWidth
                                onChange={(e) => this.handleTextFieldChange(e)}
                            />
                        </Box>
                        <Box>
                            <Button variant={"contained"} className="loginBtn" href={'/auction/' + this.state.contractAddress}>
                                Start
                            </Button>
                        </Box>
                    </Box>
                </Box>
            </div>
        );
    }
}

export default Welcome;