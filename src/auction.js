import React from 'react'
import { Box, Button, Card, TextField } from "@mui/material";
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import './auction.css'
import porsche from './porsche.png'
import Web3 from "web3";
import bidAbi from "./Contracts/Bid_abi";

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
class Auction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            account: null,
            beneficiary: 'Iris group member',
            itemName: 'Porsche 911 (992) GT3',
            countDown: 'no count down yet',
            intervalId: null,
            countDownDate: null,
            highestBid: 0,
            highestBidder: null,
            web3: null,
            contract: null,
            accountTextFieldValue: "",
            bidValue: "",
            open: false,
            errorMsg: '',
            open2: false,
            balance:null
        }
        this.web3 = new Web3("ws://127.0.0.1:7545");
        this.contract = new this.web3.eth.Contract(bidAbi, window.location.href.split('/')[4]);
        this.contract.defaultBlock = "pending";

    }
    componentDidMount() {
        this.contract.methods.beneficiary().call().then((res) => {
            this.setState({ beneficiary: res });
        });
        this.contract.methods.auctionEndTime().call().then((res) => {
            this.setState({ countDownDate: res * 1000 });
            this.getCountDown();
        });
        this.contract.methods.highestBid().call().then((res) => {
            this.setState({ highestBid: res });
        });

        this.contract.methods.highestBidder().call().then((res) => {
            this.setState({ highestBidder: res });
        });

        this.contract.events.EbidSuccess().on('data', event => {



            //If the highest bidder is not us, then we will try to withdraw our money
            if (event.returnValues.bidder !== this.state.account && this.state.highestBidder === this.state.account) {
                this.contract.methods.withdraw().send({from:this.state.account}).then((res) => {
                    if (res.status === true) {
                        this.web3.eth.getBalance(this.state.account).then(res => {
                            console.log(res);
                            this.setState({balance:res});
                        });
                        alert("Someone outbidded us, withdrawing previous bids...");
                    }
                })
            }

            this.setState({ highestBid: event.returnValues.amount, highestBidder: event.returnValues.bidder });

            //Update the balance of ours
            if(this.state.account !== null){
                this.web3.eth.getBalance(this.state.account).then(res => {
                    console.log(res);
                    this.setState({balance:res});
                });
            }
        });


        this.contract.events.Eend().on('data', event => {
            alert(event.returnValues.winner + "had win the auction with highest bid of " + event.returnValues.amount);
        });

        this.intervalId = setInterval(() => {
            this.getCountDown();
        }, 1000);
    }

    getCountDown() {
        // Get today's date and time
        const now = new Date().getTime();

        // Find the distance between now and the count down date
        const distance = (this.state.countDownDate) - now;

        if (distance <= 0) {
            console.log("Auction should be ended" + distance);
            clearInterval(this.intervalId);
            this.contract.methods.end().send({ from: this.state.beneficiary }).catch((e) => console.log(e));
        }

        // Time calculations for days, hours, minutes and seconds
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        this.setState({
            countDown: days + " days " + hours + " hours "
                + minutes + " minutes " + seconds + " seconds "
        });

    }

    setAccount() {
        this.setState({ account: this.state.accountTextFieldValue });
        this.web3.eth.getBalance(this.state.accountTextFieldValue).then(res => {
            this.setState({balance:res});
        });
    }

    handleAccountTextFieldChange(e) {
        this.setState({ accountTextFieldValue: e.target.value });
    }

    handleBidTextFieldChange(e) {
        this.setState({ bidValue: e.target.value });
    }

    handleBid() {
        this.contract.methods.bid().send({ from: this.state.account, value: this.state.bidValue * 1e18 }).then(() => {
            // alert("Bid successful");
            this.setState({
                open: true
            })
        }).catch((e) => {
            // alert(e.message);
            this.setState({
                open2: true,
                errorMsg: e.message
            })
        });
    }

    renderBiddingArea() {
        if (this.state.account) {
            return (
                <div >
                    <h1>Try your luck here</h1>
                    <Box>
                        <h2> My address: {'***' + this.state.account.slice(-5)} </h2>
                    </Box>
                    <Box>
                        <h2>Balance: {this.state.balance/1e18} eth</h2>
                    </Box>
                    <Box sx={{ display: 'flex', flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
                        <h2 className={'Inline'}>
                            I want to bid
                        </h2>
                    <Box sx={{ m: 3 }}>
                            <TextField id="outlined-basic"
                                label="Your bid here"
                                variant="outlined"
                                value={this.state.bidValue}
                                onChange={(e) => this.handleBidTextFieldChange(e)}
                                helperText="amount in wei" />
                    </Box>
                        <h2 className={'Inline'}>
                            eth
                        </h2>
                    </Box>
                    <Box >
                        <Button variant={'contained'} onClick={() => this.handleBid()}>Bid</Button>
                    </Box>
                </div>
            );
        }
        else {
            return (
                <Box sx={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                    <Box sx={{ m: 3 }}>
                        <TextField variant="outlined"
                            value={this.state.accountTextFieldValue}
                            helperText="You need to provide a ethereum account address to bid"
                            fullWidth
                            onChange={(e) => this.handleAccountTextFieldChange(e)}
                            label={"Your eth account"} />
                    </Box>
                    <Box>
                        <Button variant={'contained'} onClick={() => this.setAccount()} >Confirm</Button>
                    </Box>
                </Box>
            );
        }
    }


    render() {
        return (
            <Box sx={{ display: 'flex',flexDirection:'column', width: '100%', justifyContent: 'center', alignItems: 'center', textAlign: 'center' }}>
                <div style={{
                    position: 'fixed',
                    width: '100vw',
                    height: '100vh',
                    left: 0,
                    top: 0
                }}>
                    <div id='stars'/>
                    <div id='stars2'/>
                    <div id='stars3'/>
                </div>

                <Snackbar anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }} open={this.state.open} autoHideDuration={6000} onClose={() => {
                    this.setState({
                        open: false
                    })
                }}>
                    <Alert onClose={() => {
                        this.setState({
                            open: false
                        })
                    }} severity="success" sx={{ width: '100%' }}>
                        Bid successful!
                    </Alert>
                </Snackbar>


                <Snackbar anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }} open={this.state.open2} autoHideDuration={6000} onClose={() => {
                    this.setState({
                        open2: false
                    })
                }}>
                    <Alert onClose={() => {
                        this.setState({
                            open2: false
                        })
                    }} severity="error" sx={{ width: '100%' }}>
                        {this.state.errorMsg}
                    </Alert>
                </Snackbar>

                <Card sx={{ width: "50%", m: 3 }}>
                    <Box>
                        <img src={porsche} className={'porsche'} alt={'porsche'} />
                    </Box>
                    <Box>
                        <Box>
                            <Button variant={'outlined'} >{this.state.countDown}</Button>
                        </Box>
                        <Box>
                            <h2>Item: <span style={{
                                color: '#666'
                            }}>{this.state.itemName}</span></h2>
                        </Box>
                        <Box>
                            <h2>Beneficiary: <span style={{
                                color: '#666'
                            }}>{"***" + this.state.beneficiary.slice(-5)}</span></h2>
                        </Box>
                        <Box>
                            <h2> Highest bid: <span style={{
                                color: '#666'
                            }}>{this.state.highestBid / 1e18} (eth)</span></h2>
                        </Box>
                        <Box>
                            <h2> Highest bidder: <span style={{
                                color: '#666'
                            }}>{this.state.highestBidder === this.state.account ? "You" : '***' + this.state.highestBidder.slice(-5)}</span> </h2>
                        </Box>
                    </Box>
                </Card>
                <Card sx={{ width: "50%", textAlign: 'center', display: 'flex', justifyContent: 'center', flexDirection: 'column', m: 3}}>
                    {this.renderBiddingArea()}
                </Card>
            </Box>
        );
    }
}

export default Auction;