//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "./Wallet.sol";

contract Bid is Wallet {
    event Ebid(address indexed bidder, uint amount);
    event Ewithdraw(address indexed bidder, uint amount);
    event Eend(address winner, uint amount);
    event EtimeContinue("time not ended")
    event EtimeOver("time ended")

    error TimeOver();
    error MoneyBelow(uint highestBid);
    error TimeContinue();
    error AuctionDone();

    address payable public beneficiary;
    address public highestBidder;
    uint public highestBid;
    uint public revealEndTime;
    uint public auctionEndTime;
    bool public ended;

    mapping(address => Bid[]) public bids;

    modifier onlyBefore(uint _time) { require(block.timestamp < _time); _; }
    modifier onlyAfter(uint _time) { require(block.timestamp > _time); _; }

    constructor(uint biddingTime, uint revealTime, address payable beneficiaryAddress) {
        auctionEndTime = block.timestamp + biddingTime;
        revealEndTime = auctionEndTime + revealTime;
        beneficiary = beneficiaryAddress;
    }

    function bid(bytes32 _blindedBid) external payable {
        require(block.timestamp < auctionEndTime, "ended");
        require(msg.value > highestBid, "the bid price is low");
        // error revert 抛出
        /*
        if(block.timestamp > auctionEndTime)
            revert TimeOver();
        if(msg.value < highestBid)
            revert MoneyBelow(highestBid);
        */

        bids[msg.sender].push(Bid({
            blindedBid: _blindedBid,
            deposit: msg.value
        }));
    }

    function placeBid(address bidder, uint value) internal
            returns (bool success)
    {
        if (value <= highestBid) {
            return false;
        }
        if (highestBidder != address(0)) {
            // Refund the previously highest bidder.
            bids[highestBidder] += highestBid;
        }
        highestBid = value;
        highestBidder = bidder;
        return true;
    }

    /// Reveal your blinded bids. You will get a refund for all
    /// correctly blinded invalid bids and for all bids except for
    /// the totally highest.
    function reveal(
        uint[] memory _values,
        bool[] memory _fake,
        bytes32[] memory _secret
    )
        public
        onlyAfter(auctionEndTime)
        onlyBefore(revealEndTime)
    {
        uint length = bids[msg.sender].length;
        require(_values.length == length);
        require(_fake.length == length);
        require(_secret.length == length);

        uint refund;
        for (uint i = 0; i < length; i++) {
            Bid storage bidToCheck = bids[msg.sender][i];
            (uint value, bool fake, bytes32 secret) =
                    (_values[i], _fake[i], _secret[i]);
            if (bidToCheck.blindedBid != keccak256(abi.encodePacked(value, fake, secret))) {
                // Bid was not actually revealed.
                // Do not refund deposit.
                continue;
            }
            refund += bidToCheck.deposit;
            if (!fake && bidToCheck.deposit >= value) {
                if (placeBid(msg.sender, value))
                    refund -= value;
            }
            // Make it impossible for the sender to re-claim
            // the same deposit.
            bidToCheck.blindedBid = bytes32(0);
        }
        msg.sender.transfer(refund);
    }

    function withdraw() external returns (bool) {
        uint amount = bids[msg.sender];
        if (amount > 0) {
            bids[msg.sender] = 0;
            if (!payable(msg.sender).send(amount)) {
                bids[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }

    function end() external {
        require(block.timestamp >= auctionEndTime, "Auction not yet ended.");
        require(!ended, "The auction has been called");
        // error revert 抛出
        /*
        if(block.timestamp < auctionEndTime)
            revert TimeContinue();
        if(ended)
            revert AuctionDone;
        */
        ended = true;
        emit Eend(highestBidder, highestBid);

        beneficiary.transfer(highestBid);

    }
}
