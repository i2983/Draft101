
//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

//The Bid.sol is inherited from Wallet.sol
import "./Wallet.sol";

contract Bid is Wallet {
    //Related events to audit changes
    event EbidSuccess(address indexed bidder, uint amount);
    event Ewithdraw(address indexed bidder, uint amount);
    event Eend(address winner, uint amount);
    event EbidNotAvaliable(string message);

    //Parameters
    address payable public beneficiary;
    //Address of bidder who offers the highest price
    address public highestBidder;
    //The amount of highest bid
    uint public highestBid;
    //End time of auction
    uint public auctionEndTime;
    //Whether the auction has ended
    bool public ended;

    //Error handling 
    error AuctionNotEnded(uint time);

    mapping(address => uint) bids;

    //A constructor to set the autionEndTime to control the end time 
    // and the beneficiary to get the beneficiary address.
    constructor(uint biddingTime, address payable beneficiaryAddress) {
        auctionEndTime = block.timestamp + biddingTime;
        beneficiary = beneficiaryAddress;
    }

    function bid() external payable {
        //Check if the bid is made before auction end time
        require(block.timestamp < auctionEndTime, "Bid has ended");
        //Check if the bid is larger than current largest bid
        require(msg.value > highestBid, "the bid price is low");

        if (highestBid != 0) {
            bids[highestBidder] += highestBid;
        }
        
        //Update
        highestBidder = msg.sender;
        highestBid = msg.value;

        emit EbidSuccess(msg.sender, msg.value);
    }

    //Get the money back from the pending place when the price is not the highest price
    function withdraw() external returns (bool) {
        uint amount = bids[msg.sender];
        if (amount > 0) {
            bids[msg.sender] = 0;
            if (!payable(msg.sender).send(amount)) {
                bids[msg.sender] = amount;
                return false;
            }
        }
        return true;
    }

    //Whether the auction should be ended
    function end() external {
        require(block.timestamp >= auctionEndTime,"Auction not yet ended");
        require(!ended, "The auction has been called");

        ended = true;
        emit Eend(highestBidder, highestBid);

        beneficiary.transfer(highestBid);

    }
}
