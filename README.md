
# A dAPP about Auction

An auction is a public sale of a commodity, and the bidder who offers the highest price will be eligible to buy it. For users to participate in online auctions securely and transparently, we create an auction DApp based on smart contracts on Ethereum blockchains. The following requirements (listed in 1.1) are fulfilled when we implement this DApp.

1 REQUIREMENTS

1.1 Our auction DApp’s core functionalities are implemented and executed within Ethereum Smart Contracts (SC).

1.2 Our DApp provides a graphical user interface (GUI) implemented with Web3.js and React.

1.3 Our DApp realizes our proposed use cases. Details of use cases will be seen in Section 1.2.

2 USE CASES

2.1 Users should be able to get the address of a wallet.

2.2 The balance of the wallet could be shown on the interface of DApp.

2.3 The Smart Contract could be successfully deployed on a testnet (e.g. Ganache).

2.4 Users could create a bid, and the bid with the highest price should get the item on sale.

2.5 The bid has to be created within the time restrictions. For example, any bids overtime should be created or considered.

2.6 The newly proposed bid of the user has to be larger than 0 Ether (> 0 Ether), and has to be larger than the current highest bid.


